package com.vancra.proyecto_pogra;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class vendedoras implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int codigo;
	@Column
	private String correo;
	@Column
	private String nombre;
	@Column
	private String apellido;
	@Column
	private LocalDate fechanacimiento;
	@Column
	private String direccionentrega;
	@Column
	private String password;
	@Column
	private String estado;
	
	@OneToMany(mappedBy = "codigovendedora", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private List<telefonos> telefono;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public LocalDate getFecha_nacimiento() {
		return fechanacimiento;
	}
	public void setFecha_nacimiento(LocalDate fechanacimiento) {
		this.fechanacimiento = fechanacimiento;
	}
	public String getDireccion_entrega() {
		return direccionentrega;
	}
	public void setDireccion_entrega(String direccionentrega) {
		this.direccionentrega = direccionentrega;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public List<telefonos> getTelefono() {
        return telefono;
    }

    public void setTelefono(List<telefonos> telefono) {
        this.telefono = telefono;
    }
    
}
