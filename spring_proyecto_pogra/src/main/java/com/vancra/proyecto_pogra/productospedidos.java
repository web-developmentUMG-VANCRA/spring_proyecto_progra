package com.vancra.proyecto_pogra;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class productospedidos implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int idpp;
	@Column
	private int cantidad;
	@Column
	private String pnombre;
	@Column
	private double pcosto;
	@Column
	private double pcostot;
	
	
	private int productoid;
	private int pedidoid;
	
	
	public int getPedidoid() {
		return pedidoid;
	}
	public void setPedidoid(int pedidoid) {
		this.pedidoid = pedidoid;
	}
	
	public void setIdpp(int idpp) {
		this.idpp = idpp;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public int getProductoid() {
		return productoid;
	}
	public void setProductoid(int productoid) {
		this.productoid = productoid;
	}
	public String getPnombre() {
		return pnombre;
	}
	public void setPnombre(String pnombre) {
		this.pnombre = pnombre;
	}
	public double getPcosto() {
		return pcosto;
	}
	public void setPcosto(double pcosto) {
		this.pcosto = pcosto;
	}
	public double getPcostot() {
		return pcostot;
	}
	public void setPcostot(double pcostot) {
		this.pcostot = pcostot;
	}
}