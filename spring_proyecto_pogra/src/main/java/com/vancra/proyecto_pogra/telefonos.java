package com.vancra.proyecto_pogra;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class telefonos implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "idtelefonos")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idtelefonos;
	@Column
	private int numero;
	//* @ManyToOne
	//*@JoinColumn(name = "codigovendedora",nullable=false)
	//*private vendedoras codigovendedora;

	private int codigovendedora;
	
	//*public telefonos(vendedoras codigovendedora) {
	//*	this.codigovendedora=codigovendedora;
	//*}
	
	public int getIdtelefonos() {
		return idtelefonos;
	}
	public void setIdtelefonos(int idtelefonos) {
		this.idtelefonos = idtelefonos;
	}
	public int getCodigovendedora() {
		return codigovendedora;
	}
	public void setCodigovendedora(int codigovendedora) {
		this.codigovendedora = codigovendedora;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	//*@JsonProperty("codigovendedora")
	//*private void unpackNested(Integer codigo) {
	//*    this.codigovendedora = new vendedoras();
	//*    codigovendedora.setCodigo(codigo);
	//*}
}
