package com.vancra.proyecto_pogra;
import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface pedidosRepository extends JpaRepository<pedidos,Serializable>{
	List <pedidos> findByVendedoracodigo(int vendedoracodigo);
}
